# Project optickoPrepoznavanjeKaraktera

Softver koji prima input iz kamere, u realnom vremenu prepoznaje karaktere i daje output. Ideja je da softver ima i dodatne opcije za razlicite vrste inputa ( kamera, video snimak, slika, itd.) kao i outputa (podesavanje tipa outputa koji je pogodan za citanje korisniku). Takodje softver bi trebalo da ima i podesavanja koja ce korisniku omoguciti da manipulise inputom radi lakseg citanja karaktera (podesavanje kontrasta, balansa bele boje, senke itd.)

## Developers

- [Nikola Bulatovic, 95/2016](https://gitlab.com/nbulatovic97)
